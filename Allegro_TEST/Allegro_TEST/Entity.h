#pragma once
#include "Sprite.h"
#include "Physics.h"
#include "Audio.h"
#include "Observer.h"
#include <math.h>

const int MAX_ENTITIES = 100;

typedef unsigned int EntityState;
const EntityState ENTITY_STATE_DESTROYED = 0;
const EntityState ENTITY_STATE_IDLE = 1 << 0;
const EntityState ENTITY_STATE_STANDING = 1 << 1;
const EntityState ENTITY_STATE_MOVING = 1 << 2;
const EntityState ENTITY_STATE_FACING_UP = 1 << 3;
const EntityState ENTITY_STATE_FACING_DOWN = 1 << 4;
const EntityState ENTITY_STATE_FACING_LEFT = 1 << 5;
const EntityState ENTITY_STATE_FACING_RIGHT = 1 << 6;
const EntityState ENTITY_STATE_GROUND = 1 << 7;
const EntityState ENTITY_STATE_JUMPING = 1 << 8;

class Entity : public Element
{
public:
	float velocityX;
	float velocityY;
	float accelerationX;
	float accelerationY;
	bool isActive;
	bool isTangible;
	Sprite sprite;
	Hitbox hitbox;
	EntityState entityState;

	Entity() : velocityX(0), velocityY(0), accelerationX(0), accelerationY(0),
		isActive(false), isTangible(false)
	{
		className = "Entity";
		entityState = ENTITY_STATE_IDLE;
	};
	virtual void update(double duration) = 0;
	virtual void handleCollision(Entity* entity, CollideSide collide, float overlap) = 0;
	virtual void sendSoundEvent(AudioID id) {}
	virtual void setAnimationCycle(int cycleState, int flagsMirror)
	{
		sprite.cycleState = cycleState;
		sprite.flagsMirror = flagsMirror;
	}
};

class Floor : public Entity
{
public:
	Floor() : Entity()
	{
		className = "Floor";
	}
	virtual void update(double duration)
	{
	}
	virtual void handleCollision(Entity* entity, CollideSide collide, float overlap)
	{
		if (entity->isTangible != true)
		{
			return;
		}
		if (collide & COLLIDE_LEFT)
		{
			velocityX = 0;
			accelerationX = 0;
		}
		if (collide & COLLIDE_RIGHT)
		{
			velocityX = 0;
			accelerationX = 0;
		}
		if (collide & COLLIDE_UP)
		{
			velocityY = 0;
			accelerationY = 0;
		}
		if (collide & COLLIDE_DOWN)
		{
			velocityY = 0;
			accelerationY = 0;
		}
	}
};



class UpdateHandler
{
public:
	int numEntities;
	double duration;
	Entity* entities[MAX_ENTITIES]; // List containing all entities.

	UpdateHandler(int durationSet)
	{
		duration = durationSet;
		numEntities = 0;
		for (int i = 0; i < MAX_ENTITIES; i++)
		{
			entities[i] = NULL;
		}
	}

	void addEntity(Entity* entity)
	{
		if (numEntities < MAX_ENTITIES)
		{
			for (int i = 0; i < MAX_ENTITIES; i++)
			{
				if (entities[i] == NULL)
				{
					entities[numEntities] = entity;
					++numEntities;
					break;
				}
			}
		}
	}

	// Remove an entity from the list, but do not delete from memory
	void removeEntity(Entity* entity)
	{
		for (int i = 0; i < MAX_ENTITIES; i++)
		{
			if (entities[i] == entity)
			{
				entities[i] = NULL;
				--numEntities;
				break;
			}
		}
	}

	// Remove an entity from the list, while also deleting from memory
	void deleteEntity(Entity* entity)
	{
		for (int i = 0; i < MAX_ENTITIES; i++)
		{
			if (entities[i] == entity)
			{
				delete entities[i];
				entities[i] = NULL;
				--numEntities;
				break;
			}
		}
	}

	void update() {
		for (int i = 0; i < numEntities; i++)
		{
			if (entities[i] == NULL) continue;
			if (!entities[i]->isActive) continue;
			entities[i]->update(duration);
			if (entities[i]->entityState == ENTITY_STATE_DESTROYED)
			{
				removeEntity(entities[i]);
			}
		}

	}

	void checkCollisions()
	{
		// Use a nested loop to check collisions between all entities
		// The last entity will not need to check against itself, so only check up to next to last.
		CollideSide collide = COLLIDE_NONE;
		float overlap = 0.0f;
		for (int iCheck = 0; iCheck < numEntities - 1; iCheck++)
		{
			if (entities[iCheck] == NULL) continue;
			if (!entities[iCheck]->isActive) continue;
			// Compare the checked entity with all entities later in the list.
			for (int iCollide = iCheck + 1; iCollide < numEntities; iCollide++)
			{
				if (entities[iCollide] == NULL) continue;
				if (!entities[iCollide]->isActive) continue;
				// If there is a collision, then each entity handles its own collision
				collide = Physics::collision(entities[iCheck]->hitbox.positionX,
					entities[iCheck]->hitbox.positionY,
					entities[iCheck]->hitbox.positionX + entities[iCheck]->hitbox.width,
					entities[iCheck]->hitbox.positionY + entities[iCheck]->hitbox.height,
					entities[iCollide]->hitbox.positionX,
					entities[iCollide]->hitbox.positionY,
					entities[iCollide]->hitbox.positionX + entities[iCollide]->hitbox.width,
					entities[iCollide]->hitbox.positionY + entities[iCollide]->hitbox.height, overlap);
				if (collide != COLLIDE_NONE)
				{
					entities[iCheck]->handleCollision(entities[iCollide], collide, overlap);
					// Flip A and B in the collision check for opposite handleCollision
					if (collide & COLLIDE_LEFT)
					{
						collide = collide & ~COLLIDE_LEFT;
						collide = collide | COLLIDE_RIGHT;
					}
					else if (collide & COLLIDE_RIGHT)
					{
						collide = collide & ~COLLIDE_RIGHT;
						collide = collide | COLLIDE_LEFT;
					}
					if (collide & COLLIDE_UP)
					{
						collide = collide & ~COLLIDE_UP;
						collide = collide | COLLIDE_DOWN;
					}
					else if (collide & COLLIDE_DOWN)
					{
						collide = collide & ~COLLIDE_DOWN;
						collide = collide | COLLIDE_UP;
					}
					entities[iCollide]->handleCollision(entities[iCheck], collide, overlap);
				}
			}
		}
	}
};