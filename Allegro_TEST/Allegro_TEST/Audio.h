#pragma once
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

const int MAX_AUDIO_EVENTS = 100;
const int MAX_SAMPLES = 200;

// Indexes in the samples array
enum AudioID
{
	AUDIO_ID_NULL = -1,
	AUDIO_ID_COLLISION = 0,
	AUDIO_ID_BEEP = 1,
};

class Audio
{
private:
	static ALLEGRO_VOICE* voice;
	static ALLEGRO_MIXER* primaryMixer;
	static ALLEGRO_MIXER* sampleMixer;
	static ALLEGRO_MIXER* musicMixer;
	static ALLEGRO_MIXER* speechMixer;

	static ALLEGRO_SAMPLE* samples[MAX_SAMPLES];
	static int numSamples;

	static ALLEGRO_AUDIO_STREAM* backgroundMusic;
	static ALLEGRO_SAMPLE_INSTANCE* playerSpeechInstance;

	static int displayWidth;
	static int displayHeight;
public:
	static void init(int width, int height)
	{
		// Initialize music and sounds
		al_install_audio();
		al_init_acodec_addon();

		voice = al_create_voice(44100, ALLEGRO_AUDIO_DEPTH_INT16, ALLEGRO_CHANNEL_CONF_2); // Sound card
		primaryMixer = al_create_mixer(44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2); // Main mixer
		sampleMixer = al_create_mixer(44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2); // Mixer for soundFX
		musicMixer = al_create_mixer(44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2);
		speechMixer = al_create_mixer(44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2);
		al_attach_mixer_to_voice(primaryMixer, voice);
		al_attach_mixer_to_mixer(sampleMixer, primaryMixer);
		al_attach_mixer_to_mixer(musicMixer, primaryMixer);
		al_attach_mixer_to_mixer(speechMixer, primaryMixer);
		al_set_mixer_gain(sampleMixer, 0.5);
		al_set_default_mixer(sampleMixer);

		numSamples = 0;
		displayWidth = width;
		displayHeight = height;
		for (int i = 0; i < MAX_SAMPLES; i++)
		{
			samples[i] = NULL;
		}
		al_reserve_samples(MAX_SAMPLES);
	}

	static void addSample(const char* filename)
	{
		if (numSamples < MAX_SAMPLES)
		{
			samples[numSamples] = al_load_sample(filename);
			numSamples++;
		}
	}

	static void initPlayerSpeech(const char* filename)
	{
		playerSpeechInstance = al_create_sample_instance(al_load_sample(filename));
		al_set_sample_instance_playmode(playerSpeechInstance, ALLEGRO_PLAYMODE_ONCE);
		al_attach_sample_instance_to_mixer(playerSpeechInstance, speechMixer);
	}

	static void setPlayerSpeech(const char* filename)
	{
		al_set_sample(playerSpeechInstance, al_load_sample(filename));
	}

	static void playPlayerSpeech(bool play)
	{
		if (play)
		{
			al_play_sample_instance(playerSpeechInstance);
		}
		else
		{
			al_stop_sample_instance(playerSpeechInstance);
		}
	}

	static void setBGMusic(const char* filename, size_t buffers, unsigned int samples, bool isLoop)
	{
		backgroundMusic = al_load_audio_stream(filename, buffers, samples);
		al_attach_audio_stream_to_mixer(backgroundMusic, musicMixer);
		al_set_audio_stream_playmode(backgroundMusic, isLoop ? ALLEGRO_PLAYMODE_LOOP : ALLEGRO_PLAYMODE_ONCE);
	}

	static void playBGMusic(bool play)
	{
		al_set_audio_stream_playing(backgroundMusic, play);
	}

	static float convertPositionToPan(float x, float displayWidth)
	{
		return (x - (displayWidth / 2.0f)) / (displayWidth - (displayWidth / 2.0f));
	}

	static ALLEGRO_SAMPLE_ID* playAudio(AudioID audioID, float gain, float x, float y, float speed, bool isLoop)
	{
		ALLEGRO_SAMPLE_ID* sampleID = NULL;
		al_play_sample(samples[audioID], gain, convertPositionToPan(x, displayWidth), 1.0, isLoop ? ALLEGRO_PLAYMODE_LOOP : ALLEGRO_PLAYMODE_ONCE, sampleID);
		return sampleID;
	}

	static void stopAudio(ALLEGRO_SAMPLE_ID* sampleID)
	{
		al_stop_sample(sampleID);
	}
};
ALLEGRO_VOICE* Audio::voice = NULL;
ALLEGRO_MIXER* Audio::primaryMixer = NULL;
ALLEGRO_MIXER* Audio::sampleMixer = NULL;
ALLEGRO_MIXER* Audio::speechMixer = NULL;
ALLEGRO_MIXER* Audio::musicMixer = NULL;
ALLEGRO_SAMPLE* Audio::samples[MAX_SAMPLES];
int Audio::numSamples = 0;
ALLEGRO_AUDIO_STREAM* Audio::backgroundMusic;
ALLEGRO_SAMPLE_INSTANCE* Audio::playerSpeechInstance = NULL;
int Audio::displayWidth = 0;
int Audio::displayHeight = 0;