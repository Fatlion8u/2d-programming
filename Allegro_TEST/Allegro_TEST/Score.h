#pragma once
#include "Observer.h"
#include <iostream>

class Score : public Element, public Observer
{
public:
	int score;
	Score() : score(0) {}
	virtual void onNotify(const Element& element, Event event)
	{
		if ((element.className == "Enemy") && (event.type == EVENT_ENEMY_DESTROYED)) {
			score += event.messageNumeric;
			std::cout << score;
		}
	}
};