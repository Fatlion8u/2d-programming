#pragma once
#include "Entity.h"
#include "Observer.h"

class Enemy : public Entity, public Observable
{
public:
	Enemy() { className = "Enemy"; }
	virtual void update(double duration)
	{

	}
	virtual void handleCollision(Entity* entity, CollideSide collide, float overlap)
	{
		// call notify(this, Event( , , );
		if (entity->isTangible)
		{
			if (entity->className == "Player")
			{
				notify(*this, Event(EVENT_ENEMY_DESTROYED, 100, ""));
				entityState = ENTITY_STATE_DESTROYED;
			}
		}
		return;
	}
};