#pragma once
#include <string>

enum EventType
{
	EVENT_ENEMY_DESTROYED,
	EVENT_PLAYER_DESTROYED
};

struct Event
{
public:
	EventType type;
	double messageNumeric;
	std::string messageString;
	Event(EventType setType, double setNumeric, std::string setString) :
		type(setType), messageNumeric(setNumeric),
		messageString(setString) {}
};

class Element
{
public:
	std::string className;
	Element() : className("Element") {}
};

class Observer {
public:
	Observer* next;
	Observer() : next(NULL) {}
	virtual ~Observer() {}
	virtual void onNotify(const Element& element, Event event) = 0;
};

class Observable {
protected:
	Observer* head;
	void notify(const Element& element, Event event)
	{
		Observer* observer = head;
		while (observer != NULL) {
			observer->onNotify(element, event);
			observer = observer->next;
		}
	}
public:
	void addObserver(Observer* observer)
	{
		observer->next = head;
		head = observer;
	}
	void removeObserver(Observer* observer)
	{
		if (head == observer)
		{
			head = observer->next;
			observer->next = NULL;
			return;
		}
		Observer* current = head;
		while (current != NULL)
		{
			if (current->next == observer)
			{
				current->next = observer->next;
				observer->next = NULL;
				return;
			}
			current = current->next;
		}
	}
};