#pragma once
#include "Entity.h"
#include "Player.h"

class Command
{
public:
	virtual ~Command() {}
	virtual void execute(Player& player, int keyState) = 0;
};

class MoveRightCommand : public Command
{
public:
	virtual void execute(Player& player, int keyState)
	{
		player.handleInput(keyState == ALLEGRO_EVENT_KEY_DOWN ? true : false, INPUT_MOVE_RIGHT);
	}
};

class MoveLeftCommand : public Command
{
public:
	virtual void execute(Player& player, int keyState)
	{
		player.handleInput(keyState == ALLEGRO_EVENT_KEY_DOWN ? true : false, INPUT_MOVE_LEFT);
	}
};

class MoveUpCommand : public Command
{
public:
	virtual void execute(Player& player, int keyState)
	{
		player.handleInput(keyState == ALLEGRO_EVENT_KEY_DOWN ? true : false, INPUT_MOVE_UP);
	}
};

class MoveDownCommand : public Command
{
public:
	virtual void execute(Player& player, int keyState)
	{
		player.handleInput(keyState == ALLEGRO_EVENT_KEY_DOWN ? true : false, INPUT_MOVE_DOWN);
	}
};

class JumpCommand : public Command
{
public:
	virtual void execute(Player& player, int keyState)
	{
		player.handleInput(keyState == ALLEGRO_EVENT_KEY_DOWN ? true : false, INPUT_JUMP);
	}
};

class InputHandler {
public:
	Command* handleInput(Player& player, int keyState, int input)
	{
		if (input == ALLEGRO_KEY_W) buttonW->execute(player, keyState);
		if (input == ALLEGRO_KEY_A) buttonA->execute(player, keyState);
		if (input == ALLEGRO_KEY_S) buttonS->execute(player, keyState);
		if (input == ALLEGRO_KEY_D) buttonD->execute(player, keyState);
		if (input == ALLEGRO_KEY_J) buttonJ->execute(player, keyState);

		return NULL;
	}

	Command* buttonW;
	Command* buttonA;
	Command* buttonS;
	Command* buttonD;
	Command* buttonJ;
};