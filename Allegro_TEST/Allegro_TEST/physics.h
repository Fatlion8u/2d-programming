#pragma once


class Hitbox
{
public:
	float relativeX, relativeY; // local coordinates of top left, where top left of entity is (0,0)
	float width, height;
	float positionX, positionY; // world coordinates of top left corner of hit box.
								// Position must be updated whenever entity position is updated.
	Hitbox() : relativeX(0), relativeY(0),
		width(0), height(0), positionX(0), positionY(0) {}
	Hitbox(float relX, float relY, float w, float h, float x, float y) :
		relativeX(relX), relativeY(relY),
		width(w), height(h), positionX(x + relX), positionY(y + relY) {}
	void updatePosition(float x, float y)
	{
		positionX = x + relativeX;
		positionY = y + relativeY;
	}
};

// Bitflags for keeping track of collision side
typedef unsigned int CollideSide;
const CollideSide COLLIDE_NONE = 0;
const CollideSide COLLIDE_LEFT = 1 << 0;
const CollideSide COLLIDE_RIGHT = 1 << 1;
const CollideSide COLLIDE_UP = 1 << 2;
const CollideSide COLLIDE_DOWN = 1 << 3;

namespace Physics
{
	const float collisionBuffer = 0;

	CollideSide collision(float Ax1, float Ay1, float Ax2, float Ay2,
		float Bx1, float By1, float Bx2, float By2, float& overlap)
	{
		CollideSide collide = COLLIDE_NONE;
		// Check for collision
		if ((Ax2 > Bx1) && (Bx2 > Ax1) &&
			(Ay2 > By1) && (By2 > Ay1))
		{
			// Determine side of collision by finding the smallest overlap.
			// Checks if the right side overlap is the smallest.
			if (((Ax2 - Bx1) < (Bx2 - Ax1)) && ((Ax2 - Bx1) < (By2 - Ay1)) && ((Ax2 - Bx1) < (Ay2 - By1)))
			{
				overlap = Ax2 - Bx1;
				collide = collide | COLLIDE_RIGHT;
			}
			// Checks if the left side overlap is the smallest.
			if (((Bx2 - Ax1) < (Ax2 - Bx1)) && ((Bx2 - Ax1) < (By2 - Ay1)) && ((Bx2 - Ax1) < (Ay2 - By1)))
			{
				overlap = Bx2 - Ax1;
				collide = collide | COLLIDE_LEFT;
			}
			// Checks if the up side overlap is the smallest.
			if (((By2 - Ay1) < (Ay2 - By1)) && ((By2 - Ay1) < (Ax2 - Bx1)) && ((By2 - Ay1) < (Bx2 - Ax1)))
			{
				overlap = By2 - Ay1;
				collide = collide | COLLIDE_UP;
			}
			// Checks if the down side overlap is the smallest.
			if (((Ay2 - By1) < (By2 - Ay1)) && ((Ay2 - By1) < (Ax2 - Bx1)) && ((Ay2 - By1) < (Bx2 - Ax1)))
			{
				overlap = Ay2 - By1;
				collide = collide | COLLIDE_DOWN;
			}
			overlap += collisionBuffer;
		}

		return collide;
	}
}