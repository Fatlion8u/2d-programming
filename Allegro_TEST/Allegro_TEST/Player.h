#pragma once
#include "Entity.h"

enum Input
{
	INPUT_MOVE_UP,
	INPUT_MOVE_DOWN,
	INPUT_MOVE_LEFT,
	INPUT_MOVE_RIGHT,
	INPUT_JUMP
};

class Player : public Entity
{
public:
	Player() : Entity()
	{
		className = "Player";
	}

	virtual void update(double duration)
	{
		// decide a velocity, pixels per millisecond
		// calculate the amount of distance that should be travelled
		// position = 0.5*acceleration*time^2 + initialVelocity + initial position
		// update the position of the sprite.
		sprite.x += 0.5 * accelerationX * (duration * duration) + velocityX * duration;
		velocityX += accelerationX * duration;

		double gravity = 9.8 / 1000000.0;
		accelerationY += gravity;
		sprite.y += 0.5 * accelerationY * (duration * duration) + velocityY * duration;
		velocityY += accelerationY * duration;

		hitbox.updatePosition(sprite.x, sprite.y);

		// Creates lag for the update, and thus the render
		/*double divisor = 543, dividend = 54837958743;
		double result = 0;
		for (int i = 0; i < 1000000; i++)
		{
		result = (dividend / divisor) + (double)i;
		}*/
	}

	// Decide what do when this entity collides with another entity
	virtual void handleCollision(Entity* entity, CollideSide collide, float overlap)
	{
		if (entity->isTangible != true)
		{
			return;
		}
		if (collide & COLLIDE_LEFT)
		{
			sprite.x += overlap;
			hitbox.updatePosition(sprite.x, sprite.y);
			if (velocityX < 0) velocityX = 0;
			if (accelerationX < 0) accelerationX = 0;
		}
		if (collide & COLLIDE_RIGHT)
		{
			sprite.x -= overlap;
			hitbox.updatePosition(sprite.x, sprite.y);
			if (velocityX > 0) velocityX = 0;
			if (accelerationX > 0) accelerationX = 0;
		}
		if (collide & COLLIDE_UP)
		{
			sprite.y += overlap;
			hitbox.updatePosition(sprite.x, sprite.y);
			if (velocityY < 0) velocityY = 0;
			if (accelerationY < 0) accelerationY = 0;
		}
		if (collide & COLLIDE_DOWN)
		{
			sprite.y -= overlap;
			hitbox.updatePosition(sprite.x, sprite.y);
			if (velocityY > 0) velocityY = 0;
			if (accelerationY > 0) accelerationY = 0;
			if ((entity->className == "Floor") && !(entityState & ENTITY_STATE_GROUND))
			{
				sendSoundEvent(AUDIO_ID_COLLISION);
			}
			transition(ENTITY_STATE_GROUND);
		}
	}

	void handleInput(bool keyDown, Input input)
	{
		switch (input)
		{
		case INPUT_MOVE_RIGHT:
			moveRight(input);
			break;
		case INPUT_MOVE_LEFT:
			moveLeft(input);
			break;
		case INPUT_MOVE_UP:
			moveUp(input);
			break;
		case INPUT_MOVE_DOWN:
			moveDown(input);
			break;
		case INPUT_JUMP:
			jump(input);
			break;
		}
	}

	void transition(EntityState appendState)
	{
		entityState = entityState | appendState;
		switch (appendState)
		{
		case ENTITY_STATE_GROUND:
			entityState = entityState & ~ENTITY_STATE_JUMPING;
			break;
		case ENTITY_STATE_STANDING:
			break;
		case ENTITY_STATE_IDLE:
			entityState = entityState & ~ENTITY_STATE_MOVING;
			break;
		case ENTITY_STATE_MOVING:
			entityState = entityState & ~ENTITY_STATE_IDLE;
			break;
		case ENTITY_STATE_FACING_UP:
			break;
		case ENTITY_STATE_FACING_DOWN:
			break;
		case ENTITY_STATE_FACING_LEFT:
			sprite.flagsMirror = ALLEGRO_FLIP_HORIZONTAL;
			break;
		case ENTITY_STATE_FACING_RIGHT:
			sprite.flagsMirror = 0;
			break;
		case ENTITY_STATE_JUMPING:
			entityState = entityState & ~ENTITY_STATE_GROUND;
			break;
		}
	}

	void moveRight(bool keyDown)
	{
		if (keyDown)
		{
			// velocityX = 0.2;
			accelerationX = 0.0002;
			transition(ENTITY_STATE_FACING_RIGHT);
		}
		else
		{
			// velocityX = 0;
			accelerationX = 0;
		}
	}

	void moveLeft(bool keyDown)
	{
		if (keyDown)
		{
			// velocityX = -0.2;
			accelerationX = -0.0002;
			transition(ENTITY_STATE_FACING_LEFT);
		}
		else
		{
			// velocityX = 0;
			accelerationX = 0;
		}
	}

	void moveUp(bool keyDown)
	{
		if (keyDown)
		{
			// velocityY = -0.2;
			accelerationY = -0.0002;
		}
		else
		{
			// velocityY = 0;
			accelerationY = 0;
		}
	}

	void moveDown(bool keyDown)
	{
		if (keyDown)
		{
			// velocityY = 0.2;
			accelerationY = 0.0002;
		}
		else
		{
			// velocityY = 0;
			accelerationY = 0;
		}
	}

	void jump(bool keyDown)
	{
		if (keyDown)
		{
			if ((entityState & ENTITY_STATE_GROUND) &&
				!(entityState & ENTITY_STATE_JUMPING))
			{
				transition(ENTITY_STATE_JUMPING);
				transition(ENTITY_STATE_MOVING);
				// velocityY = 0.2;
				accelerationY -= 0.0005;
				Audio::playPlayerSpeech(true);
			}
		}
		else
		{
			// velocityY = 0;
			accelerationY = 0;
		}
	}

	virtual void sendSoundEvent(AudioID id)
	{
		Audio::playAudio(id, 1.0, sprite.x + sprite.rotateX, sprite.y + sprite.rotateY, 1.0, false);
	}
};