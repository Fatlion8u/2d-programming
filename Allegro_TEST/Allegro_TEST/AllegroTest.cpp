#include <allegro5/allegro.h> // Include main Allegro library.
#include <allegro5/allegro_image.h>
#include "Sprite.h"
#include "Entity.h"
#include "InputHandler.h"
#include "Player.h"
#include <iostream>
#include <chrono>

using namespace std::chrono;

const float FPS = 60.0;
void initSystem(ALLEGRO_DISPLAY *&display, ALLEGRO_EVENT_QUEUE *&event_queue, ALLEGRO_TIMER *&timer);
void createPlayerSprite(SpriteCycle &walkSpriteCycle1, SpriteCycle &walkSpriteCycle2, Sprite &catSprite);
void renderSprite(Sprite* sprite);

const double MS_PER_UPDATE = 4;

int main()
{
	al_init(); // Initialize Allegro library.
	al_init_image_addon();

	// Initialization
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	initSystem(display, event_queue, timer);

	Audio::init(640, 480);
	Audio::addSample("sound/tone.wav");
	Audio::addSample("sound/tone.wav");
	Audio::setBGMusic("music/Flying_Jellyfishes.flac", 4, 2048, true);
	Audio::initPlayerSpeech("speech/CatMeow.wav");

	// Create a player sprite.
	SpriteCycle walkSpriteCycle1, walkSpriteCycle2;
	Sprite catSprite;
	createPlayerSprite(walkSpriteCycle1, walkSpriteCycle2, catSprite);
	int catSpriteCycleState = 0;
	Player player;
	player.sprite = catSprite; // Set sprite for player
	player.sprite.x = 300; // Initial position
	player.sprite.y = 50;
	player.sprite.rotateX = 32;
	player.sprite.rotateY = 32;
	player.velocityX = 0;
	player.velocityY = 0;
	player.hitbox = Hitbox(0, 0, 64, 64, player.sprite.x, player.sprite.y);
	player.isActive = true;
	player.isTangible = true;

	// Create a ground entity.
	SpriteCycle grassCycle;
	grassCycle.addFrame(SpriteFrame(0, 0, 250, 50, al_load_bitmap("sprites/grass.png")));
	Sprite grassSprite;
	grassSprite.spriteCycles.push_back(grassCycle);
	Floor grass;
	grass.sprite = grassSprite;
	grass.sprite.x = 250; // Initial position
	grass.sprite.y = 300;
	grass.sprite.rotateX = 125;
	grass.sprite.rotateY = 25;
	grass.velocityX = 0;
	grass.velocityY = 0;
	grass.hitbox = Hitbox(0, 0, 250, 50, grass.sprite.x, grass.sprite.y);
	grass.isActive = true;
	grass.isTangible = true;

	// Create update handler.
	UpdateHandler updateHandler(MS_PER_UPDATE);
	updateHandler.addEntity(&player);
	updateHandler.addEntity(&grass);

	// Create input handler.
	InputHandler inputHandler;
	// Register keyboard as an event source.
	al_install_keyboard();
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	// Set commands for inputHandler
	inputHandler.buttonD = new MoveRightCommand();
	inputHandler.buttonA = new MoveLeftCommand();
	inputHandler.buttonW = new MoveUpCommand();
	inputHandler.buttonS = new MoveDownCommand();
	inputHandler.buttonJ = new JumpCommand();

	ALLEGRO_EVENT event;

	al_start_timer(timer);

	Audio::playBGMusic(true);

	steady_clock::time_point previous = steady_clock::now(), current;
	double lag = 0.0;

	while (1) {
		al_wait_for_event(event_queue, &event);

		// Calculating lag since last update.
		current = steady_clock::now();
		auto elapsed = current - previous; // Amount of time since last loop.
		previous = current;
		lag += duration_cast<milliseconds>(elapsed).count();

		// Process Input
		if ((event.type == ALLEGRO_EVENT_KEY_DOWN)
			|| (event.type == ALLEGRO_EVENT_KEY_UP))
		{
			inputHandler.handleInput(player, event.type, event.keyboard.keycode);
		}

		if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE) break;

		// Update
		while (lag >= MS_PER_UPDATE)
		{
			// update
			updateHandler.checkCollisions();
			updateHandler.update();

			lag -= MS_PER_UPDATE;
		}

		// Render
		if (event.type == ALLEGRO_EVENT_TIMER)
		{
			al_clear_to_color(al_map_rgb(200, 220, 150));

			// Draw my sprites.
			for (int i = 0; i < updateHandler.numEntities; i++)
			{
				if (updateHandler.entities[i] == NULL) continue;
				if (!updateHandler.entities[i]->isActive) continue;
				renderSprite(&(updateHandler.entities[i]->sprite));
			}

			al_flip_display();
		}
	}

	// Garbage collection
	al_destroy_timer(timer);
	al_destroy_display(display);
	al_destroy_event_queue(event_queue);
	return 0;
}

void initSystem(ALLEGRO_DISPLAY *&display, ALLEGRO_EVENT_QUEUE *&event_queue, ALLEGRO_TIMER *&timer)
{
	display = al_create_display(640, 480); // Initialize display.

	event_queue = al_create_event_queue(); // Create event queue
										   // Registers the display as an event source.
	al_register_event_source(event_queue, al_get_display_event_source(display));

	timer = al_create_timer(1.0 / FPS);
	// Registers the timer as an event source.
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
}

void createPlayerSprite(SpriteCycle &walkSpriteCycle1, SpriteCycle &walkSpriteCycle2, Sprite &catSprite)
{
	walkSpriteCycle1.addFrame(SpriteFrame(0, 0, 64, 64, al_load_bitmap("sprites/MySprite_Walk_00.png")));
	walkSpriteCycle1.addFrame(SpriteFrame(0, 0, 64, 64, al_load_bitmap("sprites/MySprite_Walk_01.png")));
	walkSpriteCycle1.addFrame(SpriteFrame(0, 0, 64, 64, al_load_bitmap("sprites/MySprite_Walk_02.png")));
	walkSpriteCycle1.addFrame(SpriteFrame(0, 0, 64, 64, al_load_bitmap("sprites/MySprite_Walk_03.png")));

	walkSpriteCycle2.addFrame(SpriteFrame(0, 0, 64, 64, al_load_bitmap("sprites/MySprite_Walk_Sheet.png")));
	walkSpriteCycle2.addFrame(SpriteFrame(64, 0, 64, 64, al_load_bitmap("sprites/MySprite_Walk_Sheet.png")));
	walkSpriteCycle2.addFrame(SpriteFrame(128, 0, 64, 64, al_load_bitmap("sprites/MySprite_Walk_Sheet.png")));
	walkSpriteCycle2.addFrame(SpriteFrame(192, 0, 64, 64, al_load_bitmap("sprites/MySprite_Walk_Sheet.png")));

	// Add the animation cycles to this sprite.
	catSprite.spriteCycles.push_back(walkSpriteCycle1);
	catSprite.spriteCycles.push_back(walkSpriteCycle2);
}

void renderSprite(Sprite* sprite)
{
	if (sprite->cycleState != sprite->cycleStatePrev) sprite->resetCycle();
	// Apply any transformations to mySprite here before rendering the frame.

	// Render the sprite.
	al_draw_tinted_scaled_rotated_bitmap_region(sprite->getCurrentFrame().frameBitmap,
		sprite->getCurrentFrame().x, sprite->getCurrentFrame().y,
		sprite->getCurrentFrame().w, sprite->getCurrentFrame().h,
		al_map_rgb(255, 255, 255),
		sprite->rotateX, sprite->rotateY,
		sprite->x + sprite->rotateX, sprite->y + sprite->rotateY,
		sprite->scaleX, sprite->scaleY, sprite->angle,
		sprite->flagsMirror);
	// Advance the animation cycle frame.
	sprite->advanceFrame();
}