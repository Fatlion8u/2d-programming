/*
This header file is for keeping track of sprites and animation cycles.
*/

#pragma once
#include <string>
#include <vector>
#include <allegro5/allegro.h> // Include main Allegro library.
#include <allegro5/allegro_image.h>

/* Represents a single frame of animation for a sprite. */
struct SpriteFrame
{
	float x, y; // top-left position of the frame on the single image or spritesheet.
	float w, h; // width and height of the frame.

	std::string imageName; // The name of the image file for this sprite frame.
	ALLEGRO_BITMAP *frameBitmap;

	/* Initializer, no arguments. */
	SpriteFrame() : x(0), y(0), w(0), h(0), imageName("") {}
	/* Initializer, all arguments. */
	SpriteFrame(float X, float Y, float W, float H, ALLEGRO_BITMAP *inBitmap)
		: x(X), y(Y), w(W), h(H), frameBitmap(inBitmap) {}
};

/* Represents a full animation cycle, with the ability to keep track of and retrieve the current
frame being used. */
class SpriteCycle {
public:
	std::vector<SpriteFrame> spriteFrames; // Frames describing a cycle of animation
	int frameNumber; // Current frame number, for use with spriteFrames

					 /* Initializer */
	SpriteCycle() : frameNumber(0) {}

	/* Gets the current frame of animation in this cycle. */
	SpriteFrame getCurrentFrame() { return spriteFrames[frameNumber]; }

	/* Advances the cycle by one frame. */
	void advanceFrame() { frameNumber = (frameNumber + 1) % spriteFrames.size(); }
	/* Resets the cycle to the first frame. */
	void resetCycle() { frameNumber = 0; }
	/* Add a frame to this animation cycle. */
	void addFrame(SpriteFrame frame) { spriteFrames.push_back(frame); }
};

/* Class to keep track of the current state of a sprite. */
class Sprite {
public:
	float x, y; // Position of the sprite
	int flagsMirror; // Keeps track of whether the sprite is mirrored.
	float scaleX, scaleY, rotateX, rotateY, angle; // Transformations
	int cycleState; // Current cycle, for use with referencing spriteCycles.
	int cycleStatePrev;
	std::vector<SpriteCycle> spriteCycles; // Contains all animation cycles for this sprite.

										   /* Initializer that sets everything to default values. */
	Sprite() : x(0), y(0), flagsMirror(0), scaleX(1), scaleY(1),
		angle(0), cycleState(0), cycleStatePrev(0) {}
	/* Gets the current frame of animation. */
	SpriteFrame getCurrentFrame() { return spriteCycles[cycleState].getCurrentFrame(); }
	/* Advance the frame of the current cycle. */
	void advanceFrame() { spriteCycles[cycleState].advanceFrame(); }
	/* Reset the frame of the current cycle. */
	void resetCycle() { spriteCycles[cycleState].resetCycle(); cycleStatePrev = cycleState; }
	void setCycle(int newCycle) { cycleStatePrev = cycleState; cycleState = newCycle; }
};